#pragma once
#include "Task.h"
#include <queue>
#include <iostream>
using namespace std;

class Scheduler
{
public:

};

struct sortByR
{
	bool operator()(const Task a, const Task b) const
	{
		return (a.r < b.r);
	}
};

struct sortQueueByLowestR
{
	bool operator()(const Task a, const Task b) const
	{
		return (a.r > b.r);
	}
};

struct sortByQ
{
	bool operator()(const Task a, const Task b) const
	{
		return (a.q > b.q);
	}
};

struct sortQueueByHighestQ
{
	bool operator()(const Task a, const Task b) const
	{
		return (a.q < b.q);
	}
};

struct results
{
	vector<int> taskOrder;
	int Cmax;
};

results schrage(int n, priority_queue<Task, vector<Task>, sortQueueByLowestR> allTasks, priority_queue<Task, vector<Task>, sortQueueByHighestQ> readyTasks);

results sortRQ(int n, vector<Task>& taskList);

results optimal(int n, vector<Task>& taskList, priority_queue<Task, vector<Task>, sortQueueByLowestR> allTasks, priority_queue<Task, vector<Task>, sortQueueByHighestQ> readyTasks);
