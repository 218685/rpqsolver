#include "Scheduler.h"

results schrage(int n, priority_queue<Task, vector<Task>, sortQueueByLowestR> allTasks,	priority_queue<Task, vector<Task>, sortQueueByHighestQ> readyTasks)
{
	int t = 0, k = 0, Cmax = 0;
	vector<int> taskOrder;
	Task tempTask(0, 0, 0, 0);

	while (!allTasks.empty() || !readyTasks.empty())
	{
		while (!allTasks.empty() && allTasks.top().r <= t)
		{
			tempTask = allTasks.top();
			readyTasks.push(tempTask);
			allTasks.pop();
		}
		if (readyTasks.empty())
		{
			t = allTasks.top().r;
			continue;
		}
		tempTask = readyTasks.top();
		readyTasks.pop();

		k++;
		taskOrder.push_back(tempTask.id);
		t += tempTask.p;

		Cmax = max(Cmax, t + tempTask.q);
	}
	
	results toReturn;
	toReturn.Cmax = Cmax;
	toReturn.taskOrder = taskOrder;

	return toReturn;
}

results sortRQ(int n, vector<Task>& taskList)
{
	priority_queue<Task, vector<Task>, sortQueueByLowestR> A;
	priority_queue<Task, vector<Task>, sortQueueByHighestQ> B;

	for (int i = 0; i < taskList.size(); ++i)
	{
		if (taskList[i].r <= taskList[i].q)
			A.push(taskList[i]);
		else
			B.push(taskList[i]);
	}

	taskList.clear();
	vector<int> taskOrder;
	Task  tempTask(0, 0, 0, 0);

	while (!A.empty())
	{
		tempTask = A.top();
		taskOrder.push_back(tempTask.id);
		taskList.push_back(tempTask);
		A.pop();
	}
	while (!B.empty())
	{
		tempTask = B.top();
		taskOrder.push_back(tempTask.id);
		taskList.push_back(tempTask);
		B.pop();
	}

	int Cmax = 0, t = 0;

	for (int i = 0; i < taskList.size(); i++)
	{
		t = max(taskList[i].r, t) + taskList[i].p;
		Cmax = max(Cmax, t + taskList[i].q);
	}

	results toReturn;
	toReturn.Cmax = Cmax;
	toReturn.taskOrder = taskOrder;

	return toReturn;
}

results optimal(int n, vector<Task>& taskList, 
	priority_queue<Task, vector<Task>, sortQueueByLowestR> allTasks,
	priority_queue<Task, vector<Task>, sortQueueByHighestQ> readyTasks)
{
	results schrageResults = schrage(n, allTasks, readyTasks);
	results sortRQResults = sortRQ(n, taskList);
	
	results toReturn;

	if (schrageResults.Cmax < sortRQResults.Cmax)
	{
		toReturn.Cmax = schrageResults.Cmax;
		toReturn.taskOrder = schrageResults.taskOrder;
	}
	else
	{
		toReturn.Cmax = sortRQResults.Cmax;
		toReturn.taskOrder = sortRQResults.taskOrder;
	}

	return toReturn;

}