#include "Task.h"

Task::Task(int r1, int p1, int q1, int number)
{
	r = r1;
	p = p1;
	q = q1;
	id = number;
}


Task::~Task()
{
}


std::ostream& operator<< (std::ostream& stream, const Task& task)
{
	stream << task.id << " | " << task.r << " " << task.p << " " << task.q;
	return stream;
}
