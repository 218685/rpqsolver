// rpqSolver.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Scheduler.h"
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <Windows.h>
using namespace std;

int main(int argc, char **argv)
{
	__int64 start, stop, freq;
	QueryPerformanceCounter((LARGE_INTEGER*)&start);

	fstream rpqData, rpqResults;
	string  fileName;
	string fileNames[] = { "data1.txt", "data2.txt", "data3.txt", "data4.txt" };

	int numberOfTasks, taskNumber = 0, r, p, q;
	vector<Task> taskList;
	priority_queue<Task, vector<Task>, sortQueueByLowestR > allTasks;

	int sortRSum = 0, sortQSum = 0, sortSchrageSum = 0 , sortNewSum=0, sortOptimalSum = 0;

	rpqResults.open("rpqResults.txt", ios::out);
	if (rpqResults.good())
	{
		for (int i = 0; i < 4; i++)
		{
			fileName = fileNames[i];
			cout << "\n" << fileName << endl;
			rpqResults << "\n" << fileName << endl;
			rpqData.open(fileName, ios::in);
			if (rpqData.good())
			{
				rpqData >> numberOfTasks;
				//wczytanie zadan
				while (!rpqData.eof())
				{
					rpqData >> r >> p >> q;
					taskNumber++;
					Task task_i(r, p, q, taskNumber);
					taskList.push_back(task_i);
					allTasks.push(task_i);
				}
				
				/*
				for (int i = 0; i < taskList.size(); i++)
				{
					cout << taskList[i] << endl;
				}
				
				while (!allTasks.empty())
				{
					cout << allTasks.top() << endl;
					allTasks.pop();
				}*/
				rpqData.close();
			}
			else
				cerr << "Could not open the file: " << fileName << "!\n";

			priority_queue<Task, vector<Task>, sortQueueByHighestQ> readyTasks;
			//optimal
			results optimalResults = optimal(numberOfTasks, taskList, allTasks, readyTasks);
			cout << "Optimal \n";
			rpqResults << "Optimal \n";
			
			for (int i = 0; i < optimalResults.taskOrder.size(); ++i)
			{
				cout << optimalResults.taskOrder[i] << " ";
				rpqResults << optimalResults.taskOrder[i] << " ";
			}
			cout << "\nCmax = " << optimalResults.Cmax << "\n";
			rpqResults << "\nCmax = " << optimalResults.Cmax << "\n";
			sortOptimalSum += optimalResults.Cmax;

			allTasks = priority_queue<Task, vector<Task>, sortQueueByLowestR>();
			readyTasks = priority_queue<Task, vector<Task>, sortQueueByHighestQ>();
			taskNumber = 0;
			/*
			//sortR;

			sort(taskList.begin(), taskList.end(), sortByR());

			cout << "SortR:" << endl;
			rpqResults << "SortR \n";

			for (int i = 0; i < taskList.size(); i++)
			{
				cout << taskList[i].id << " ";
				rpqResults << taskList[i].id << " ";
			}
			
			int Cmax = 0, t = 0;

			for (int i = 0; i < taskList.size(); i++)
			{
				t = max(taskList[i].r, t) + taskList[i].p;
				Cmax = max(Cmax, t + +taskList[i].q);
			}

			cout << "\nCmax = " << Cmax << "\n";
			rpqResults << "\nCmax = " << Cmax << "\n";
			sortRSum += Cmax;
			*/
			//sortQ;
			taskList.clear();
		}
		/*
		cout << "\nSuma sortR: " << sortRSum;
		rpqResults << "\nSuma sortR: " << sortRSum;
		
		cout << "\nSuma sortQ: " << sortQSum;
		rpqResults << "\nSuma sortQ: " << sortQSum;
		
		cout << "\nSuma sortSchrage: " << sortSchrageSum;
		rpqResults << "\nSuma sortSchrage: " << sortSchrageSum;

		cout << "\nSuma sortNew: " << sortNewSum;
		rpqResults << "\nSuma sortNew: " << sortNewSum;
		*/
		cout << "\nSuma sortopt: " << sortOptimalSum;
		rpqResults << "\nSuma sortopt: " << sortOptimalSum;

		QueryPerformanceCounter((LARGE_INTEGER*)&stop);
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		cout << "\nCzas dzialania: " << ((double)(stop - start)) / freq << " s" << endl;
		rpqResults << "\nCzas dzialania: " << ((double)(stop - start)) / freq << " s" << endl;
		rpqResults.close();
	}
	else
		cerr << "Could not create the file: rpqResults.txt!\n";
	

	system("PAUSE");
    return 0;
}


