#pragma once
#include <ostream>

class Task
{

public:
	int r, p, q, id;
	Task(int r1, int p1, int q1, int number);
	~Task();
};

std::ostream& operator<< (std::ostream& stream, const Task& task);

